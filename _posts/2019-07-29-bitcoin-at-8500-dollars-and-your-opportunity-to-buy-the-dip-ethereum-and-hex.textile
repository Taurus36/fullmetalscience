---
vim: spell spelllang=en
layout: post
class: post-template
#cover: https://img.youtube.com/vi/vchHIRxfiDg/0.jpg
cover: assets/images/vchHIRxfiDg.jpg
title: "Bitcoin at $8500 and your opportunity to buy the dip! Ethereum and HEX."
source: https://www.youtube.com/watch?v=vchHIRxfiDg
tags: [Quotes]
author: rheart
---

We watched this two-and-a-half hour YouTube stream by Richard Heart and extracted the highlights for ourselves ... and you!


h2. Quotes

p. _Telling the truth about this project and the parts that suck about it, does not mean the price is going down. Right, you can be truthful and understand that fixing these things makes the price go up even more._ - https://www.youtube.com/watch?v=vchHIRxfiDg#t=7m49s

p. _Pseudo law enforcement through the reduction of civil liberties is retarded and we are supposed to have, you know, laws to protect our rights. If you can't transact with another human being without a middleman, that's slavery! There's no reason for someone else to have control over what you do with your money._ - https://www.youtube.com/watch?v=vchHIRxfiDg#t=16m31s

p. _If you short anything, you might make money, but you ain't gonna make more than a 100%. If you short at 20k, if you took twenty thousand dollars and you shorted twenty thousand dollars and you covered at zero, you'd make twenty thousand. That's 1x._ - https://www.youtube.com/watch?v=vchHIRxfiDg#t=32m26s

p. _Do you want to have untraceable? Well, we have Monero now, and it works, so why would you hope and dream about some other thing that might work or, you know, might inflation bug everything?_ - https://www.youtube.com/watch?v=vchHIRxfiDg#t40m21s

p. _The only interesting thing that you could add to Bitcoin would be anonymous transactions, and I don't think you'll ever get them rolled into the code, ever, because everyone will lose their bank accounts, right? All these people like Coinbase and exchanges._ - https://www.youtube.com/watch?v=vchHIRxfiDg#t=42m41s

p. _So when people talk about how shitty crypto is `cause you can't spend it, I'm like "who cares?". You can't spend Coca Cola stock either, but the price keeps going up._ - https://www.youtube.com/watch?v=vchHIRxfiDg#t=1h25m32s

p. _If you need anything else, [blockchain] is useless garbage. is the slowest, most unreliable, most expensive way to do anything in computing. Literally, seriously, it's that bad. But it's censorship resistent._ - https://www.youtube.com/watch?v=vchHIRxfiDg#t=1h29m38s

p. _People think that a supply gap somehow helps your value. It doesn't. Let me say this again: Bitcoin has high inflation and it's had higher inflation than the US dollar for the entirety of its existence up until maybe the last couple o'months._ - https://www.youtube.com/watch?v=vchHIRxfiDg#t=2h18m34s

p. _So, it doesn't matter what your inflation rate is, it matters what your inflation rate to new users is._ - https://www.youtube.com/watch?v=vchHIRxfiDg#t=2h19m01s
